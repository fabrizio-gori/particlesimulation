package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomSimulationBuilder extends AbstractSimulationBuilder {
	
	private final Random random;
	private final double radius;
	private final int size;
	
	public RandomSimulationBuilder(double coefficientOfRestitution, long seed, double radius, int size, int x, int y, int width, int height) {
		
		super(coefficientOfRestitution, x, y, width, height);
		
		random = new Random(seed);
		this.radius = radius;
		this.size = size;
	}

	@Override
	public void buildParticles() {
		
		final List<Particle> result = new ArrayList<Particle>();
		
		double mass = 1.0;
		
		int numX = size, numY = size;
		double deltaX = getWidth() / numX;
		double deltaY = getHeight() / numY;
		
		for(int i=0; i<numX; i++) {
			
			for(int j=0; j<numY; j++) {
			
				Vector2d vel;
				
				try {
					
					vel = new Vector2d(-1 + 2.0 * random.nextDouble(), -1 + 2.0 * random.nextDouble()).normalize();
					
				} catch(ArithmeticException e) {
					
					vel = new Vector2d(0, 0);
				}
				
				vel = vel.multiply(1/6.0);
				
				Vector2d pos = new Vector2d(getX() + (deltaX * i + radius + (deltaX - (2 * radius)) * random.nextDouble()), getY() + (deltaY * j + radius + (deltaY - (2 * radius)) * random.nextDouble()));
				Particle p = new Particle(pos, vel, radius, mass);
				
				result.add(p);
				p.setColor(new Color((int)(255.0 * random.nextDouble()), 0, (int)(255.0 * random.nextDouble())));
			}
		}
		
		getSimulation().setParticles(result);
	}
}
