package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class Test2SimulationBuilder extends AbstractSimulationBuilder {

	public Test2SimulationBuilder(double coefficientOfRestitution, int x, int y, int width, int height) {
		
		super(coefficientOfRestitution, x, y, width, height);
	}

	@Override
	public void buildParticles() {
		
		List<Particle> result = new ArrayList<Particle>(3);
		
		result.add(new Particle(new Vector2d(getX() + 150, getY() + 120), new Vector2d( 0.5, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 180), new Vector2d(-0.5, 0), 30.0, 10.0));	
		
		result.get(0).setColor(Color.MAGENTA);
		result.get(1).setColor(Color.CYAN);
		
		getSimulation().setParticles(result);
	}
}