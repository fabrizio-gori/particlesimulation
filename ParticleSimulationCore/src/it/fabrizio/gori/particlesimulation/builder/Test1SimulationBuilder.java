package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class Test1SimulationBuilder extends AbstractSimulationBuilder {

	public Test1SimulationBuilder(double coefficientOfRestitution, int x, int y, int width, int height) {
		
		super(coefficientOfRestitution, x, y, width, height);
	}
	
	@Override
	public void buildParticles() {
		
		List<Particle> result = new ArrayList<Particle>();
		
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 450), new Vector2d(0, -0.5), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 280), new Vector2d(0, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 220), new Vector2d(0, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 160), new Vector2d(0, 0), 30.0, 1.0));

		result.add(new Particle(new Vector2d(getX() + 70,  getY() + 90), new Vector2d(0, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 130, getY() + 90), new Vector2d(0, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 35), new Vector2d(0, 0), 30.0, 1.0));

		result.get(0).setColor(Color.CYAN);
		result.get(1).setColor(Color.MAGENTA);
		result.get(2).setColor(Color.YELLOW);
		result.get(3).setColor(Color.BLUE);
		
		getSimulation().setParticles(result);
	}
}
