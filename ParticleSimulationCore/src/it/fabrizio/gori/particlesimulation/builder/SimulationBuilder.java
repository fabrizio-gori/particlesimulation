package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.Simulation;

public interface SimulationBuilder {
	
	public void createNewSimulation();
	public void buildParticles();
	public void buildWalls();
	public void buildCoefficientOfRestitution();
	public void buildCollisionManager();
	public Simulation getSimulation();
}
