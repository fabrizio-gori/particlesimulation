package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class Test6SimulationBuilder extends AbstractSimulationBuilder {

	public Test6SimulationBuilder(double coefficientOfRestitution, int x, int y, int width, int height) {
		
		super(coefficientOfRestitution, x, y, width, height);
	}

	@Override
	public void buildParticles() {
		
		List<Particle> result = new ArrayList<Particle>();
		
		result.add(new Particle(new Vector2d(getX() + 150, getY() + 150), new Vector2d(0, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 150, getY() + 150 - 42), new Vector2d(0, 0), 12.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 150 + 39 * Math.cos(Math.PI / 3), getY() + 150 - 39 * Math.sin(Math.PI / 3)), new Vector2d(0, 0), 9.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 150 - 39 * Math.cos(Math.PI / 3), getY() + 150 - 39 * Math.sin(Math.PI / 3)), new Vector2d(0, 0), 9.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 150 + 36 * Math.cos(Math.PI / 6), getY() + 150 - 36 * Math.sin(Math.PI / 6)), new Vector2d(0, 0), 6.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 150 - 36 * Math.cos(Math.PI / 6), getY() + 150 - 36 * Math.sin(Math.PI / 6)), new Vector2d(0, 0), 6.0, 1.0));

		result.add(new Particle(new Vector2d(getX() + 150, getY() + 300), new Vector2d(0, -0.5), 30.0, 1.0));
		
		result.get(0).setColor(Color.CYAN);
		result.get(1).setColor(Color.MAGENTA);
		result.get(2).setColor(Color.YELLOW);
		result.get(3).setColor(Color.BLUE);
		result.get(4).setColor(Color.YELLOW);
		result.get(5).setColor(Color.BLUE);
		result.get(6).setColor(Color.CYAN);
		
		getSimulation().setParticles(result);
	}
}
