package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.Simulation;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;
import it.fabrizio.gori.particlesimulation.physics.Wall;
import it.fabrizio.gori.particlesimulation.physics.collision.CollisionManagerImpl;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSimulationBuilder implements SimulationBuilder {
	
	private double x, y, width, height;
	private double coefficientOfRestitution;
	private Simulation simulation;
	
	public AbstractSimulationBuilder(double coefficientOfRestitution, double x, double y, double width, double height) {

		this.coefficientOfRestitution = coefficientOfRestitution;
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	@Override
	public void createNewSimulation() {
		
		simulation = new Simulation();
	}
	
	@Override
	public void buildWalls() {
		
		List<Wall> result = new ArrayList<Wall>(4);
		
		result.add(new Wall(new Vector2d(x, y), new Vector2d(x + width, y), new Vector2d(0, 1)));
		result.add(new Wall(new Vector2d(x, y + height), new Vector2d(x + width, y + height), new Vector2d(0, -1)));
		result.add(new Wall(new Vector2d(x, y), new Vector2d(x, y + height), new Vector2d(1, 0)));
		result.add(new Wall(new Vector2d(x + width, y), new Vector2d(x + width, y + height), new Vector2d(-1, 0)));
	
		simulation.setWalls(result);
	}
	
	@Override
	public void buildCoefficientOfRestitution() {
		
		simulation.setCoefficientOfRestitution(coefficientOfRestitution);
	}
	
	@Override
	public void buildCollisionManager() {
		
		simulation.setCollisionManager(new CollisionManagerImpl(simulation));
	}
	
	@Override
	public Simulation getSimulation() {
		
		return simulation;
	}
	
	protected double getX() {
		
		return x;
	}
	
	protected double getY() {
		
		return y;
	}
	
	protected double getWidth() {
		
		return width;
	}
	
	protected double getHeight() {
		
		return height;
	}
}
