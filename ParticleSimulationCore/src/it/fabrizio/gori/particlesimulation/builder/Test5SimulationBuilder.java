package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class Test5SimulationBuilder extends AbstractSimulationBuilder {

	public Test5SimulationBuilder(double coefficientOfRestitution, int x, int y, int width, int height) {
		
		super(coefficientOfRestitution, x, y, width, height);
	}

	@Override
	public void buildParticles() {
		
		List<Particle> result = new ArrayList<Particle>(3);
		
		result.add(new Particle(new Vector2d(getX() + 100, getY() + 100), new Vector2d(-0.5, -0.5), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 180, getY() + 180), new Vector2d(0.5, 0.5), 30.0, 1.0));
		
		getSimulation().setParticles(result);
	}
}