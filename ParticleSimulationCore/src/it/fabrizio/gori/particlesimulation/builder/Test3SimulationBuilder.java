package it.fabrizio.gori.particlesimulation.builder;

import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

import java.util.ArrayList;
import java.util.List;

public class Test3SimulationBuilder extends AbstractSimulationBuilder {

	public Test3SimulationBuilder(double coefficientOfRestitution, int x, int y, int width, int height) {
		
		super(coefficientOfRestitution, x, y, width, height);
	}

	@Override
	public void buildParticles() {
		
		List<Particle> result = new ArrayList<Particle>(3);
		
		result.add(new Particle(new Vector2d(getX() + 181, getY() + 150), new Vector2d(0, 0), 30.0, 1.0));
		result.add(new Particle(new Vector2d(getX() + 119, getY() + 150), new Vector2d(0, 0), 30.0, 10.0));
		result.add(new Particle(new Vector2d(getX() + 150, getY() + 250), new Vector2d(0, -0.5), 30.0, 5.0));
		
		getSimulation().setParticles(result);
	}
}