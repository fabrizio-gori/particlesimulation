package it.fabrizio.gori.particlesimulation;

import it.fabrizio.gori.particlesimulation.graphics.Drawable;
import it.fabrizio.gori.particlesimulation.graphics.Renderer;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Wall;
import it.fabrizio.gori.particlesimulation.physics.collision.Collision;
import it.fabrizio.gori.particlesimulation.physics.collision.CollisionManager;

import java.util.Collections;
import java.util.List;

public class Simulation implements Drawable {
	
	private List<Particle> particles;
	private List<Wall> walls;
	private double coefficientOfRestitution;
	private double currentTimestamp, startTimestamp;
	private double momentum = 0;
	private Collision<?, ?> currentCollision;
	private CollisionManager collisionManager;
	
	public List<Particle> getParticles() {
		
		return Collections.unmodifiableList(particles);
	}
	
	public void setParticles(List<Particle> particles) {
	
		this.particles = particles;
		
		momentum = 0;
		
		for(Particle i : particles) {
			
			momentum += i.getVelocity().lengthSquared();
		}
	}

	public List<Wall> getWalls() {
	
		return Collections.unmodifiableList(walls);
	}
	
	public void setWalls(List<Wall> walls) {
		
		this.walls = walls;
	}

	public double getCoefficientOfRestitution() {
		
		return coefficientOfRestitution;
	}
	
	public void setCoefficientOfRestitution(double coefficientOfRestitution) {
		
		this.coefficientOfRestitution = coefficientOfRestitution;
	}

	public void setCollisionManager(CollisionManager collisionManager) {
		
		this.collisionManager = collisionManager;
	}

	public double getCurrentTimestamp() {
	
		return currentTimestamp;
	}
	
	public void draw(Renderer renderer) {
		
		renderer.render(this);
	}
	
	public void start(double startTimestamp) {
		
		this.startTimestamp = startTimestamp;
		this.currentTimestamp = 0;
		
		currentCollision = collisionManager.getNextCollision();
	}
	
	public boolean isCompleted() {
		
		return (currentCollision == null) || momentum == 0;
	}
	
	public void update(double timestamp) {
		
		currentTimestamp = (timestamp - startTimestamp);
		
		while(currentCollision != null && currentCollision.getTime() <= currentTimestamp) {
			
			momentum += currentCollision.resolve();
			
			currentCollision = collisionManager.getNextCollision();
		}
	}
	
	public double getMomentum() {
		
		return momentum;
	}
}
