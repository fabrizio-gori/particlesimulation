package it.fabrizio.gori.particlesimulation;

import it.fabrizio.gori.particlesimulation.builder.SimulationBuilder;


public class SimulationCreator {

	private final SimulationBuilder simulationBuilder;
	
	public SimulationCreator(SimulationBuilder simulationBuilder) {
		
		this.simulationBuilder = simulationBuilder;
	}
	
	public Simulation createSimulation() {
		
		simulationBuilder.createNewSimulation();
		simulationBuilder.buildWalls();
		simulationBuilder.buildParticles();
		simulationBuilder.buildCoefficientOfRestitution();
		simulationBuilder.buildCollisionManager();
		
		return simulationBuilder.getSimulation();
	}
	
	public Simulation getSimulation() {
		
		return simulationBuilder.getSimulation();
	}
}
