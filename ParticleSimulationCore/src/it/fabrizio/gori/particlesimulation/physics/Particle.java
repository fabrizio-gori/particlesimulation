package it.fabrizio.gori.particlesimulation.physics;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.graphics.Drawable;
import it.fabrizio.gori.particlesimulation.graphics.Renderer;

public class Particle implements Drawable, Collidable {

	private static int nextId = 0;

	private Vector2d position;
	private Vector2d velocity;
	private double radius;
	private double mass;
	private double lastUpdateTimestamp = 0;
	private int id;
	private Color color = Color.BLUE;
	
	public Particle(Vector2d position, Vector2d velocity, double radius, double mass) {
	
		this.position = position;
		this.velocity = velocity;
		this.radius = radius;
		this.mass = mass;
		
		this.id = nextId;
		
		nextId++;
	}
	
	public String getName() {
		
		return "Particle " + id;
	}
	
	public Vector2d getPosition() {
		
		return position;
	}

	public void setPosition(Vector2d position) {
		
		this.position = position;
	}
	
	public Vector2d getVelocity() {
		
		return velocity;
	}

	public void setVelocity(Vector2d velocity) {
		
		this.velocity = velocity;
	}

	public double getRadius() {
		
		return radius;
	}

	public double getMass() {
		
		return mass;
	}
	
	public void setColor(Color color) {
		
		this.color = color;
	}
	
	public Color getColor() {
	
		return color;
	}
	
	public double getLastUpdateTimestamp() {
	
		return lastUpdateTimestamp;
	}

	public void updatePosition(double timestamp) {
		
		double dt = timestamp - lastUpdateTimestamp;
		
		position = position.add(velocity.multiply(dt));
		
		lastUpdateTimestamp = timestamp;
	}

	@Override
	public void draw(Renderer renderer) {
		
		renderer.render(this);
	}

	@Override
	public String toString() {
		
		return getName();
	}

	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Particle other = (Particle) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
