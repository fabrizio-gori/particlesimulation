package it.fabrizio.gori.particlesimulation.physics;

import it.fabrizio.gori.particlesimulation.graphics.Drawable;
import it.fabrizio.gori.particlesimulation.graphics.Renderer;


public class Wall implements Drawable, Collidable {

	private static int nextId = 0;
	
	private final Vector2d start;
	private final Vector2d end;
	private final Vector2d normal;
	private int id;
	
	public Wall(Vector2d start, Vector2d end, Vector2d normal) {
		
		this.start = start;
		this.end = end;
		this.normal = normal;
		
		this.id = nextId;
		
		nextId++;
	}
	
	public String getName() {
		
		return "Wall " + id;
	}

	public Vector2d getNormal() {
	
		return normal;
	}
	
	public Vector2d getStart() {
		
		return start;
	}

	public Vector2d getEnd() {
		
		return end;
	}
	
	@Override
	public void draw(Renderer renderer) {
		
		renderer.render(this);
	}
	
	@Override
	public String toString() {
		
		return getName();
	}

	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Wall other = (Wall) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
