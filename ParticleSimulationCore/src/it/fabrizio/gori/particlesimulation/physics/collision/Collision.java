package it.fabrizio.gori.particlesimulation.physics.collision;

import it.fabrizio.gori.particlesimulation.physics.Collidable;

public interface Collision<A extends Collidable, B extends Collidable> extends Comparable<Collision<A, B>> {
	
	public A getA();
	public B getB();
	public double getTime();
	public double resolve();
}
