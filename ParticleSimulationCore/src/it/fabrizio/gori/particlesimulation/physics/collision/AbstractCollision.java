package it.fabrizio.gori.particlesimulation.physics.collision;

import it.fabrizio.gori.particlesimulation.physics.Collidable;

public abstract class AbstractCollision<A extends Collidable, B extends Collidable> implements Collision<A, B> {
	
	private final A a;
	private final B b;
	private final double time;
	private final CollisionManager collisionManager;
	
	public AbstractCollision(CollisionManager collisionManager, A a, B b, double time) {
		
		this.collisionManager = collisionManager;
		this.a = a;
		this.b = b;
		this.time = time;
	}
	
	@Override
	public final A getA() {
	
		return a;
	}
	
	@Override
	public final  B getB() {
		
		return b;
	}
	
	@Override
	public final double getTime() {
		
		return time;
	}
	
	@Override
	public int compareTo(Collision<A, B> other) {
		
		int result = 0;
		
		if(getTime() > other.getTime()) {
			
			result = 1;
			
		} else if(getTime() < other.getTime()) {
			
			result = -1;
		}
		
		return result;
	}

	@Override
	public String toString() {
		
		return "[" + getA() + " - " + getB() + " (" + getTime() + ")]";
	}

	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		long temp;
		temp = Double.doubleToLongBits(time);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractCollision<?, ?> other = (AbstractCollision<?, ?>) obj;

		if(Double.doubleToLongBits(time) != Double.doubleToLongBits(other.time))
			return false;
		
		if(a != null && b != null && other.a != null && other.b != null) {
		
			if((a.equals(other.a) && b.equals(other.b)) ||
			   (a.equals(other.b) && b.equals(other.a))) {
				
				return true;
			}
		}
		
		if(a == null && b == null && other.a == null && other.b == null) {
			
			return true;
		}
		
		return false;
	}
	
	protected final CollisionManager getCollisionManager() {
		
		return collisionManager;
	}
}
