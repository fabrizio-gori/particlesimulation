package it.fabrizio.gori.particlesimulation.physics.collision;

import it.fabrizio.gori.particlesimulation.Simulation;

public interface CollisionManager {
	
	public void removeCollision(ParticleParticleCollision collision);
	public void removeCollision(ParticleWallCollision collision);
	public Collision<?, ?> getNextCollision();
	public Simulation getSimulation();
}
