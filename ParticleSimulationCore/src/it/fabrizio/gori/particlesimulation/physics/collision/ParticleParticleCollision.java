package it.fabrizio.gori.particlesimulation.physics.collision;

import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;

public final class ParticleParticleCollision extends AbstractCollision<Particle, Particle> {
	
	public ParticleParticleCollision(CollisionManager collisionManager, Particle a, Particle b, double time) {
		
		super(collisionManager, a, b, time);
	}
	
	@Override
	public double resolve() {
		
		double result = getA().getVelocity().lengthSquared() + getB().getVelocity().lengthSquared();
		
		getA().updatePosition(getTime());
		getB().updatePosition(getTime());
		
		changeVelocity();
		
		result = (getA().getVelocity().lengthSquared() + getB().getVelocity().lengthSquared()) - result;
		
		getCollisionManager().removeCollision(this);
		
		return result;
	}
	
	private void changeVelocity() {
		
		Vector2d mtd;
		
		try {
			
			mtd = getA().getPosition().subtract(getB().getPosition()).normalize();
			
		} catch(ArithmeticException e) {
			
			mtd = new Vector2d(0, 0);
		}
		
		Vector2d v = getA().getVelocity().subtract(getB().getVelocity());
		
		double im1 =  1 / getA().getMass();
		double im2 = -1 / getB().getMass();
		double vn = v.dot(mtd);

		if(vn <= 0.0) {
			
			double i = (-(1.0 + getCollisionManager().getSimulation().getCoefficientOfRestitution()) * vn) / (im1 - im2);
			Vector2d impulse = mtd.multiply(i);
			
			getA().setVelocity(getA().getVelocity().add(impulse.multiply(im1)));
			getB().setVelocity(getB().getVelocity().add(impulse.multiply(im2)));
		}
	}
}
