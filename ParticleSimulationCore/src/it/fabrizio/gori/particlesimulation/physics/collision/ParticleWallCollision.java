package it.fabrizio.gori.particlesimulation.physics.collision;

import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;
import it.fabrizio.gori.particlesimulation.physics.Wall;

public final class ParticleWallCollision extends AbstractCollision<Particle, Wall> {
	
	public ParticleWallCollision(CollisionManager collisionManager, Particle a, Wall b, double time) {
		
		super(collisionManager, a, b, time);
	}

	@Override
	public double resolve() {
		
		getA().updatePosition(getTime());
		
		changeVelocity();
		
		getCollisionManager().removeCollision(this);
		
		return 0;
	}
	
	private void changeVelocity() {

		Vector2d newVelocity = new Vector2d(getB().getNormal());
		double dot = getA().getVelocity().dot(getB().getNormal());
		
		newVelocity = newVelocity.multiply(-2 * dot).add(getA().getVelocity());
		
		getA().setVelocity(newVelocity);
	}
}
