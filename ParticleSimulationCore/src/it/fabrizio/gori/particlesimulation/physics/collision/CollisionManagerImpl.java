package it.fabrizio.gori.particlesimulation.physics.collision;

import it.fabrizio.gori.particlesimulation.Simulation;
import it.fabrizio.gori.particlesimulation.physics.Collidable;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Vector2d;
import it.fabrizio.gori.particlesimulation.physics.Wall;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CollisionManagerImpl implements CollisionManager {

	private class PriorityQueue<E extends Comparable<E>> implements Iterable<E> {
	
		private ArrayList<E> heap = null;

		public PriorityQueue(int initialCapacity) {
			
			heap = new ArrayList<E>(initialCapacity);
		}
		
		@Override
		public Iterator<E> iterator() {
			
			return heap.iterator();
		}
		
		public boolean addAll(Collection<? extends E> c) {
			
			return heap.addAll(c);
		}
		
		public E poll() {

			if (heap.size() == 0) {
				
				return null;
			}

			makeHeap(0);

			E value = heap.get(0);
			E lastValue = heap.remove(heap.size() - 1);
			
			if(heap.size() > 0) {
				
				heap.set(0, lastValue);
			}
			
			return value;
		}

		public boolean contains(Object o) {
			
			return heap.contains(o);
		}
		
		private void makeHeap(int node) {

			if (isLeaf(node)) {
			
				return;
			}
			
			makeHeap(getLeftChild(node));
			
			int rightChild = getRightChild(node);
			
			if (rightChild < heap.size()) {
			
				makeHeap(rightChild);
			}
			
			mergeHeaps(node);
		}

		private void mergeHeaps(int node) {
			
			int heapSize = heap.size();
			E value = heap.get(node);

			while (!isLeaf(node, heapSize)) {
			
				int smallestChild = getSmallestChild(node, heapSize);
				
				if (value.compareTo(heap.get(smallestChild)) < 0) {
				
					break;
				}
				
				heap.set(node, heap.get(smallestChild));
				node = smallestChild;
			}
			
			heap.set(node, value);
		}

		private int getLeftChild(int node) {
			
			return 2 * node + 1;
		}

		private int getRightChild(int node) {
			
			return 2 * node + 2;
		}

		private boolean isLeaf(int node, int size) {
			
			return node * 2 + 1 >= size;
		}
		
		private boolean isLeaf(int node) {
			
			return isLeaf(node, heap.size());
		}
		
		private int getSmallestChild(int node, int heapSize) {
			
			int smallestChild;
			int leftChild = getLeftChild(node);
			int rightChild = leftChild + 1;
			smallestChild = leftChild;
			
			if ((rightChild < heapSize) && (heap.get(rightChild).compareTo(heap.get(leftChild)) < 0)) {
				
				smallestChild = rightChild;
			}
			
			return smallestChild;
		}
	}

	private static final double EPSILON = 1E-5;
	
	private final Simulation simulation;
	private final PriorityQueue<Collision<?, ?>> collisions;
	private final Set<Particle> toBeRecalculatedSet = new HashSet<Particle>();
	
	public CollisionManagerImpl(Simulation simulation) {
		
		this.simulation = simulation;
		
		collisions = new PriorityQueue<Collision<?, ?>>(simulation.getParticles().size());
		
		for(Particle i : simulation.getParticles()) {
			
			collisions.addAll(checkCollision(i, null));
		}
	}
	
	@Override
	public Simulation getSimulation() {
		
		return simulation;
	}

	@Override
	public Collision<?, ?> getNextCollision() {
		
		return collisions.poll();
	}

	@Override
	public void removeCollision(ParticleParticleCollision collision) {
		
		toBeRecalculatedSet.clear();
		
		Iterator<Collision<?, ?>> iter = collisions.iterator();
		while(iter.hasNext()) {
			
			Collision<?, ?> i = iter.next();
			
			if(i.getTime() > collision.getTime()) {

				if(i.getA().equals(collision.getA()) || 
				   i.getB().equals(collision.getA()) ||
				   i.getA().equals(collision.getB()) ||
				   i.getB().equals(collision.getB())) {
					
					toBeRecalculated(i.getA());
					toBeRecalculated(i.getB());
					
					iter.remove();
				}
			}
		}
		
		toBeRecalculated(collision.getA());
		toBeRecalculated(collision.getB());
		
		for(Particle i : toBeRecalculatedSet) {
			
			collisions.addAll(checkCollision(i, collision));
		}
	}
	
	@Override
	public void removeCollision(ParticleWallCollision collision) {
		
		toBeRecalculatedSet.clear();
		
		Iterator<Collision<?, ?>> iter = collisions.iterator();
		while(iter.hasNext()) {
			
			Collision<?, ?> i = iter.next();
			
			if(i.getTime() > collision.getTime()) {

				if(i.getA().equals(collision.getA()) || 
				   i.getB().equals(collision.getA())) {
					
					toBeRecalculated(i.getA());
					toBeRecalculated(i.getB());
					
					iter.remove();
				}
			}
		}
		
		toBeRecalculated(collision.getA());
		
		for(Particle i : toBeRecalculatedSet) {
			
			collisions.addAll(checkCollision(i, collision));
		}
	}

	private void toBeRecalculated(Collidable collidable) {
		
		if(collidable instanceof Particle) {
			
			toBeRecalculatedSet.add((Particle)collidable);
		}
	}
	
	private List<Collision<?, ?>> checkCollision(Particle p, Collision<?, ?> collisionRemoved) {
		
		List<Collision<?, ?>> result = new ArrayList<Collision<?, ?>>();
		
		for(Particle i : getSimulation().getParticles()) {
			
			if(!i.equals(p)) {

				Collision<?, ?> c = getCollision(p, i);
				
				if(c != null && !collisions.contains(c) && !c.equals(collisionRemoved)) {
					
					if(result.isEmpty()) {
						
						result.add(c);
					
					} else {
	
						if(c.getTime() < result.get(0).getTime()) {
							
							result.clear();
							result.add(c);
						
						} else if(c.getTime() == result.get(0).getTime()) {
							
							result.add(c);
						}
					}
				}
			}
		}

		for(Wall i : getSimulation().getWalls()) {
			
			if(!i.equals(p)) {

				Collision<?, ?> c = getCollision(p, i);
				
				if(c != null && !collisions.contains(c) && !c.equals(collisionRemoved)) {
					
					if(result.isEmpty()) {
						
						result.add(c);
					
					} else {
	
						if(c.getTime() < result.get(0).getTime()) {
							
							result.clear();
							result.add(c);
						
						} else if(c.getTime() == result.get(0).getTime()) {
							
							result.add(c);
						}
					}
				}
			}
		}
		
		return result;
	}
	
	private Collision<?, ?> getCollision(Particle a, Wall w) {
		
		Collision<?, ?> result = null;
		
		double cx = a.getPosition().getX(),
			   cy = a.getPosition().getY(),
			  
			   px = w.getStart().getX(),
			   py = w.getStart().getY(),
			  
			   nx = w.getNormal().getX(),
			   ny = w.getNormal().getY(),
			  
			   vx = a.getVelocity().getX(),
			   vy = a.getVelocity().getY(),
			  
			   num = -cx*nx - cy*ny + px*nx + py*ny + a.getRadius(),
			   den = vx*nx + vy*ny;
		
		double t;
		
		if(a.getVelocity().dot(w.getNormal()) <= 0 && den != 0) {
		
			t = num / den;
			
			result = new ParticleWallCollision(this, a, w, a.getLastUpdateTimestamp() + t);
		}
		
		return result;
	}
	
	private Collision<?, ?> getCollision(Particle p1, Particle p2) {
		
		Collision<?, ?> result = null;

		double dtMax = Math.max(p1.getLastUpdateTimestamp(), p2.getLastUpdateTimestamp());
		
		p1.updatePosition(dtMax);
		p2.updatePosition(dtMax);
		
		double ax   = p1.getPosition().getX(),
			   ay   = p1.getPosition().getY(),
			   bx   = p2.getPosition().getX(),
			   by   = p2.getPosition().getY(),
		       vax  = p1.getVelocity().getX(),
			   vay  = p1.getVelocity().getY(),
			   vbx  = p2.getVelocity().getX(),
			   vby  = p2.getVelocity().getY();

		Vector2d mtd;
		
		try {
			
			mtd = new Vector2d(ax - bx, ay - by).normalize();
			
		} catch(ArithmeticException e) {
			
			mtd = new Vector2d(0, 0);
		}
		
		Vector2d v = new Vector2d(vax - vbx, vay - vby);
		double vn = v.dot(mtd);
				
		if(vn <= -EPSILON) {

			double ax2  = ax * ax,
				   ay2  = ay * ay,
				   bx2  = bx * bx,
				   by2  = by * by,
				   vax2 = vax * vax,
				   vay2 = vay * vay,
				   vbx2 = vbx * vbx,
				   vby2 = vby * vby,
				  
				   rab = p1.getRadius() + p2.getRadius();
			
			double a = vax2 + vay2 + vbx2 + vby2 - 2 * (vax*vbx + vay*vby),
				   b = 2 * (ax*vax + ay*vay + bx*vbx + by*vby - ax*vbx - bx*vax - ay*vby - by*vay),
			       c = ax2 + ay2 + bx2 + by2 - 2 * (ax*bx + ay*by) - (rab*rab);
		
			double delta = b*b - 4*a*c;
		
			double t;
			
			if(a != 0 && delta > 0) {
				
				double t1, t2;
				
				t1 = (-b + Math.sqrt(delta)) / (2*a);
				t2 = (-b - Math.sqrt(delta)) / (2*a);
	
				if(t1 < 0 && t2 < 0) return null;
				
				if(Math.abs(t1) <= Math.abs(t2)) t = t1;
				else t = t2;
	
				if(t < 0) t = 0;
				
				result = new ParticleParticleCollision(this, p1, p2, dtMax + t);
			}
		}
		
		return result;
	}
}
