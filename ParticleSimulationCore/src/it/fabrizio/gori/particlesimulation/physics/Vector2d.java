package it.fabrizio.gori.particlesimulation.physics;

public class Vector2d {

	public static final Vector2d ZERO = new Vector2d(0, 0);
	
	private final double x;
	private final double y;

	public Vector2d() {
		
		x = 0;
		y = 0;
	}
	
	public Vector2d(Vector2d other) {
		
		x = other.x;
		y = other.y;
	}

	public Vector2d(double x, double y) {
		
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		
		return x;
	}

	public double getY() {
		
		return y;
	}

	public double dot(Vector2d v2) {
		
		double result = 0.0;
		
		result = x * v2.x + y * v2.y;
		
		return result;
	}

	public double length() {
		
		return Math.sqrt(x * x + y * y);
	}
	
	public double lengthSquared() {
		
		return x * x + y * y;
	}

	public double getDistance(Vector2d v2) {
		
		return Math.sqrt((v2.x - x) * (v2.x - x) + (v2.y - y) * (v2.y - y));
	}

	public Vector2d add(Vector2d v2) {
		
		return new Vector2d(x + v2.x, y + v2.y);
	}

	public Vector2d subtract(Vector2d v2) {
		
		return new Vector2d( x - v2.x, y - v2.y);
	}

	public Vector2d multiply(double scaleFactor) {
		
		return new Vector2d(x * scaleFactor, y * scaleFactor);
	}

	public Vector2d normalize() {
		
		double len = length();
		
		if (len != 0.0) {
			
			return new Vector2d(x / len, y / len);
			
		} else {
			
			throw new ArithmeticException("Cannot normalize a zero vector.");
		}
	}

	@Override
	public String toString() {
		
		return "(" + x + ", " + y + ")";
	}

	@Override
	public int hashCode() {
		
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2d other = (Vector2d) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}