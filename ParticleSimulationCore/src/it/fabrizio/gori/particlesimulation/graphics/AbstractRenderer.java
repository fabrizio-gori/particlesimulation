package it.fabrizio.gori.particlesimulation.graphics;

public abstract class AbstractRenderer implements Renderer {
	
	private GraphicContext graphicContext;

	@Override
	public GraphicContext getGraphicContext() {
		
		return graphicContext;
	}
	
	@Override
	public void setGraphicContext(GraphicContext graphicContext) {
		
		this.graphicContext = graphicContext;
	}
}
