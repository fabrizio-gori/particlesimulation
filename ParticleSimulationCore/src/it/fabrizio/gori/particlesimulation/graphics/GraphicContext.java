package it.fabrizio.gori.particlesimulation.graphics;

public interface GraphicContext {
	
	public int getWidth();
	public int getHeight();
	public void clear();
	public void drawCircle(double cx, double cy, double radius, boolean fill);
	public void drawLine(double ax, double ay, double bx, double by);
	public void drawText(String text, double x, double y, double ang);
	public void setColor(Color color);
}
