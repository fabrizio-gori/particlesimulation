package it.fabrizio.gori.particlesimulation.graphics;

import it.fabrizio.gori.particlesimulation.Simulation;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Wall;

public class StandardRenderer extends AbstractRenderer {

	private double currentTimestamp;
	private final boolean simulationInfo;
	
	public StandardRenderer() {
		
		this(true);
	}
	
	public StandardRenderer(boolean simulationInfo) {
		
		this.simulationInfo = simulationInfo; 
	}
	
	@Override
	public void render(Simulation simulation) {
		
		getGraphicContext().clear();
		
		currentTimestamp = simulation.getCurrentTimestamp();
		
		for(Particle i : simulation.getParticles()) {
			
			i.draw(this);
		}
		
		for(Wall i : simulation.getWalls()) {
			
			i.draw(this);
		}
		
		if(simulationInfo) {
			
			getGraphicContext().setColor(Color.BLACK);
			getGraphicContext().drawText("COR=" + simulation.getCoefficientOfRestitution(), 10, getGraphicContext().getHeight() - 10, 0);
			getGraphicContext().drawText("TIME=" + (int)(simulation.getCurrentTimestamp() / 1000.0) + "s", 10, getGraphicContext().getHeight() - 20, 0);
	
			getGraphicContext().setColor(Color.RED);
			getGraphicContext().drawText("MOMENTUM=" + simulation.getMomentum(), 10, getGraphicContext().getHeight() - 30, 0);
	
			if(simulation.isCompleted()) {
				
				getGraphicContext().setColor(Color.BLACK);
				getGraphicContext().drawText("Simulaton complete", 10, getGraphicContext().getHeight() - 40, 0);
			}
		}
	}

	@Override
	public void render(Wall wall) {
		
		getGraphicContext().setColor(Color.BLACK);
		getGraphicContext().drawLine(wall.getStart().getX(), wall.getStart().getY(), wall.getEnd().getX(), wall.getEnd().getY());
	}

	@Override
	public void render(Particle particle) {

		double dt = getCurrentTimestamp() - particle.getLastUpdateTimestamp();
		
		double cx = particle.getPosition().getX() + particle.getVelocity().getX() * dt, 
			   cy = particle.getPosition().getY() + particle.getVelocity().getY() * dt;
		
		getGraphicContext().setColor(particle.getColor());
		getGraphicContext().drawCircle(cx, cy, particle.getRadius(), true);
	}
	
	protected double getCurrentTimestamp() {
	
		return currentTimestamp;
	}
}
