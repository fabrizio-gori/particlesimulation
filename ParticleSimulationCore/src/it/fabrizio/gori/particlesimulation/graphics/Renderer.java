package it.fabrizio.gori.particlesimulation.graphics;

import it.fabrizio.gori.particlesimulation.Simulation;
import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Wall;

public interface Renderer {
	
	public void render(Wall wall);
	public void render(Particle particle);
	public void render(Simulation simulation);
	public GraphicContext getGraphicContext();
	public void setGraphicContext(GraphicContext graphicContext);
}
