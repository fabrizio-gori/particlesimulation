package it.fabrizio.gori.particlesimulation.graphics;

public interface Drawable {
	
	public void draw(Renderer renderer);
}
