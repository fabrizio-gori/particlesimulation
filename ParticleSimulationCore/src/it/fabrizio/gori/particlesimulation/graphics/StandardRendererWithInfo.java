package it.fabrizio.gori.particlesimulation.graphics;

import it.fabrizio.gori.particlesimulation.physics.Particle;
import it.fabrizio.gori.particlesimulation.physics.Wall;

public class StandardRendererWithInfo extends StandardRenderer {

	private final boolean particleInfo, wallInfo;
	
	public StandardRendererWithInfo() {
	
		this(true, true, true);
	}
	
	public StandardRendererWithInfo(boolean simulationInfo, boolean particleInfo, boolean wallInfo) {
		
		super(simulationInfo);
		
		this.particleInfo = particleInfo;
		this.wallInfo = wallInfo;
	}
	
	@Override
	public void render(Wall wall) {
		
		super.render(wall);
		
		double x = wall.getStart().getX() + 20.0 * wall.getNormal().getX(),
		       y = wall.getStart().getY() + 20.0 * wall.getNormal().getY(),
		       midX = (wall.getStart().getX() + wall.getEnd().getX()) / 2.0,
		       midY = (wall.getStart().getY() + wall.getEnd().getY()) / 2.0,
		       ang;
		
		getGraphicContext().setColor(Color.GREEN);
		getGraphicContext().drawLine(midX, midY, midX + 10.0 * wall.getNormal().getX(), midY + 10.0 * wall.getNormal().getY());
		
		if(wall.getNormal().getY() == 0) {
			
			ang = (Math.PI / 2.0);
			y += 10;
			getGraphicContext().setColor(Color.BLACK);
			getGraphicContext().drawText(wall.getName(), x, y, ang);

			if(wallInfo) {
				
				getGraphicContext().drawText("n=" + wall.getNormal().toString(), x - 10, y, (Math.PI / 2.0));
			}
			
		} else {
		
			ang = 0;
			x += 50;
			getGraphicContext().setColor(Color.BLACK);
			getGraphicContext().drawText(wall.getName(), x, y, ang);
			
			if(wallInfo) {

				getGraphicContext().drawText("n=" + wall.getNormal().toString(), x, y + 10, ang);
			}
		}
	}
	
	@Override
	public void render(Particle particle) {

		super.render(particle);
		
		double dt = getCurrentTimestamp() - particle.getLastUpdateTimestamp();
		
		double cx = particle.getPosition().getX() + particle.getVelocity().getX() * dt, 
			   cy = particle.getPosition().getY() + particle.getVelocity().getY() * dt;

		double r2 = particle.getRadius() * 10;

		getGraphicContext().setColor(Color.BLACK);
		getGraphicContext().drawLine(particle.getPosition().getX(),
				                     particle.getPosition().getY(),
				                     cx, cy);

		getGraphicContext().setColor(Color.RED);
		getGraphicContext().drawLine(cx, cy, cx + r2 * particle.getVelocity().getX(), cy + r2 * particle.getVelocity().getY());

		getGraphicContext().setColor(Color.BLACK);
		getGraphicContext().drawText(particle.getName(), cx, cy, 0);

		if(particleInfo) {

			getGraphicContext().drawText("p=" + particle.getPosition().toString(), cx, cy+10, 0);
			getGraphicContext().drawText("v=" + particle.getVelocity().toString(), cx, cy+20, 0);
			getGraphicContext().drawText("r=" + particle.getRadius() + ", m=" + particle.getMass(), cx, cy+30, 0);
		}
	}
}
