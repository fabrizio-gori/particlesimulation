package it.fabrizio.gori.particlesimulation.swing;

import it.fabrizio.gori.particlesimulation.builder.RandomSimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test1SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test2SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test3SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test4SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test5SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test6SimulationBuilder;
import it.fabrizio.gori.particlesimulation.graphics.StandardRenderer;
import it.fabrizio.gori.particlesimulation.graphics.StandardRendererWithInfo;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class Application {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Particle Simulation");

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setIconImage(new ImageIcon("ParticleSimulation.png").getImage());
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		JScrollPane scrollPane = new JScrollPane();
		frame.getContentPane().add(scrollPane);
				
		JPanel panel = new JPanel();
		
		panel.setLayout(new GridLayout(4, 4));
		panel.setBackground(Color.WHITE);
		
		scrollPane.setViewportView(panel);
		
		panel.add(new JSimulationComponent(new Test1SimulationBuilder(0, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false), 300, 500));
		panel.add(new JSimulationComponent(new Test1SimulationBuilder(1, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false), 300, 500));
		panel.add(new JSimulationComponent(new Test6SimulationBuilder(0, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false), 300, 500));
		panel.add(new JSimulationComponent(new Test6SimulationBuilder(1, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false), 300, 500));

		panel.add(new JSimulationComponent(new Test2SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));
		panel.add(new JSimulationComponent(new Test2SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));
		panel.add(new JSimulationComponent(new Test3SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));
		panel.add(new JSimulationComponent(new Test3SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));

		panel.add(new JSimulationComponent(new Test4SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));
		panel.add(new JSimulationComponent(new Test4SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));
		panel.add(new JSimulationComponent(new Test5SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));
		panel.add(new JSimulationComponent(new Test5SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false), 300, 300));

		long seed = (long)(Long.MAX_VALUE * Math.random());
		panel.add(new JSimulationComponent(new RandomSimulationBuilder(0, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer(), 300, 300));
		panel.add(new JSimulationComponent(new RandomSimulationBuilder(0.33, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer(), 300, 300));
		panel.add(new JSimulationComponent(new RandomSimulationBuilder(0.66, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer(), 300, 300));
		panel.add(new JSimulationComponent(new RandomSimulationBuilder(1, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer(), 300, 300));
		
		frame.pack();
		frame.setLocationRelativeTo(null);
        frame.setVisible(true);
	}
}
