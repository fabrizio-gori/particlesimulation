package it.fabrizio.gori.particlesimulation.swing;

import it.fabrizio.gori.particlesimulation.Simulation;
import it.fabrizio.gori.particlesimulation.SimulationCreator;
import it.fabrizio.gori.particlesimulation.builder.SimulationBuilder;
import it.fabrizio.gori.particlesimulation.graphics.Renderer;
import it.fabrizio.gori.particlesimulation.swing.graphics.GraphicContextSwing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

public class JSimulationComponent extends JComponent {
	
	private static final long serialVersionUID = 1L;
	
	private final Renderer renderer;
	private final Simulation simulation;
	private double time = 0;
	private final BufferedImage bufferedImage;
	
	public JSimulationComponent(SimulationBuilder simulationBuilder, Renderer renderer, int width, int height) {

		setPreferredSize(new Dimension(width + 4, height + 50));
		
		bufferedImage = new BufferedImage(width + 4, height + 50, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = (Graphics2D)bufferedImage.getGraphics();

		graphics.setClip(0, 0, width + 4, height + 50);
		graphics.setBackground(new Color(255, 255, 255));
		graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		this.renderer = renderer;
		this.renderer.setGraphicContext(new GraphicContextSwing(graphics));
		this.simulation = new SimulationCreator(simulationBuilder).createSimulation();

		simulation.start(time);
	}
	
	@Override
	public void paint(Graphics g) {
		
		simulation.update(time);
		simulation.draw(renderer);
		
		time += (1000.0 / 60.0);
		
		((Graphics2D)g).drawRenderedImage(bufferedImage, null);
		
		if(!simulation.isCompleted()) {
			
			repaint();
		}
	}
}
