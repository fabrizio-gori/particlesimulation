package it.fabrizio.gori.particlesimulation.swing.graphics;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.graphics.GraphicContext;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;

public class GraphicContextSwing implements GraphicContext {

	private final Graphics graphics;
	private final int width, height;
	
	public GraphicContextSwing(Graphics graphics) {
	
		this.graphics = graphics;
		
		this.width = graphics.getClipBounds().width;
		this.height = graphics.getClipBounds().height;
	}
	
	@Override
	public int getWidth() {
		
		return width;
	}


	@Override
	public int getHeight() {
		
		return height;
	}

	@Override
	public void clear() {
		
		graphics.clearRect(0, 0, graphics.getClipBounds().width, graphics.getClipBounds().height);
	}

	@Override
	public void drawCircle(double cx, double cy, double radius, boolean fill) {
		
		if(fill) {
		
			graphics.fillOval((int)(cx - radius), (int)(cy - radius), (int)(radius * 2), (int)(radius * 2));
		
		} else {

			graphics.drawOval((int)(cx - radius), (int)(cy - radius), (int)(radius * 2), (int)(radius * 2));
		}
	}

	@Override
	public void drawLine(double ax, double ay, double bx, double by) {
		
		graphics.drawLine((int)ax, (int)ay, (int)bx, (int)by);
	}

	@Override
	public void drawText(String text, double x, double y, double ang) {
		
		AffineTransform rotation = new AffineTransform();
		
		rotation.rotate(ang);
		
		graphics.setFont(graphics.getFont().deriveFont(rotation));
		
		graphics.drawString(text, (int)x, (int)y);
	}

	@Override
	public void setColor(Color color) {
		
		graphics.setColor(new java.awt.Color(color.getRed(), color.getGreen(), color.getBlue()));
	}
}
