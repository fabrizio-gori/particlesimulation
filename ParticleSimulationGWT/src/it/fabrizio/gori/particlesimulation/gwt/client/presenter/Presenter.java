package it.fabrizio.gori.particlesimulation.gwt.client.presenter;

import it.fabrizio.gori.particlesimulation.gwt.client.view.View;

public interface Presenter {
	
	public View<?> getView();
	public void setView(View<?> view);
	public void onBind();
}
