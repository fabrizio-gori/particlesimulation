package it.fabrizio.gori.particlesimulation.gwt.client.view;

import it.fabrizio.gori.particlesimulation.gwt.client.presenter.Presenter;

import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.AttachEvent.Handler;
import com.google.gwt.user.client.ui.Composite;

public abstract class CompositeView<T extends Presenter> extends Composite implements View<T> {

	private T presenter;
	
	public CompositeView(T presenter) {
		
		this.presenter = presenter;
		
		if(getPresenter() != null) {
			
			getPresenter().setView(this);
			
			addAttachHandler(new Handler() {
				
				@Override
				public void onAttachOrDetach(AttachEvent event) {
					
					if(getPresenter() != null && event.isAttached()) {
					
						getPresenter().onBind();
					}
				}
			});
		}
	}

	@Override
	public T getPresenter() {
		
		return presenter;
	}
}
