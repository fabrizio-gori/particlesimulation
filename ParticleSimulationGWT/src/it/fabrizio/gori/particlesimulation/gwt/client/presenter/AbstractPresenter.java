package it.fabrizio.gori.particlesimulation.gwt.client.presenter;

import it.fabrizio.gori.particlesimulation.gwt.client.view.View;

public abstract class AbstractPresenter<T extends View<? extends Presenter>> implements Presenter {
	
	private T view;

	@Override
	@SuppressWarnings("unchecked")
	public void setView(View<?> view) {
		
		this.view = (T)view;
	}
	
	@Override
	public T getView() {
		
		return view;
	}
}
