package it.fabrizio.gori.particlesimulation.gwt.client;

import it.fabrizio.gori.particlesimulation.builder.RandomSimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test1SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test2SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test3SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test4SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test5SimulationBuilder;
import it.fabrizio.gori.particlesimulation.builder.Test6SimulationBuilder;
import it.fabrizio.gori.particlesimulation.graphics.StandardRenderer;
import it.fabrizio.gori.particlesimulation.graphics.StandardRendererWithInfo;
import it.fabrizio.gori.particlesimulation.gwt.client.presenter.SimulationPresenter;
import it.fabrizio.gori.particlesimulation.gwt.client.view.SimulationViewImpl;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.RootPanel;

public class Application implements EntryPoint {
	
	@Override
	public void onModuleLoad() {
		
		FlexTable flexTable = new FlexTable();

		flexTable.setWidget(0, 0, new SimulationViewImpl(new SimulationPresenter(new Test1SimulationBuilder(0, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false)), 300, 550));
		flexTable.setWidget(0, 1, new SimulationViewImpl(new SimulationPresenter(new Test1SimulationBuilder(1, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false)), 300, 550));
		flexTable.setWidget(0, 2, new SimulationViewImpl(new SimulationPresenter(new Test6SimulationBuilder(0, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false)), 300, 550));
		flexTable.setWidget(0, 3, new SimulationViewImpl(new SimulationPresenter(new Test6SimulationBuilder(1, 0, 0, 300, 500), new StandardRendererWithInfo(true, false, false)), 300, 550));
		
		flexTable.setWidget(1, 0, new SimulationViewImpl(new SimulationPresenter(new Test2SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));
		flexTable.setWidget(1, 1, new SimulationViewImpl(new SimulationPresenter(new Test2SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));
		flexTable.setWidget(1, 2, new SimulationViewImpl(new SimulationPresenter(new Test3SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));
		flexTable.setWidget(1, 3, new SimulationViewImpl(new SimulationPresenter(new Test3SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));

		flexTable.setWidget(2, 0, new SimulationViewImpl(new SimulationPresenter(new Test4SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));
		flexTable.setWidget(2, 1, new SimulationViewImpl(new SimulationPresenter(new Test4SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));
		flexTable.setWidget(2, 2, new SimulationViewImpl(new SimulationPresenter(new Test5SimulationBuilder(0, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));
		flexTable.setWidget(2, 3, new SimulationViewImpl(new SimulationPresenter(new Test5SimulationBuilder(1, 0, 0, 300, 300), new StandardRendererWithInfo(true, false, false)), 300, 350));

		long seed = (long)(Long.MAX_VALUE * Math.random());
		flexTable.setWidget(3, 0, new SimulationViewImpl(new SimulationPresenter(new RandomSimulationBuilder(0,    seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer()), 300, 350));
		flexTable.setWidget(3, 1, new SimulationViewImpl(new SimulationPresenter(new RandomSimulationBuilder(0.33, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer()), 300, 350));
		flexTable.setWidget(3, 2, new SimulationViewImpl(new SimulationPresenter(new RandomSimulationBuilder(0.66, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer()), 300, 350));
		flexTable.setWidget(3, 3, new SimulationViewImpl(new SimulationPresenter(new RandomSimulationBuilder(0.99, seed, 7.0, 7, 0, 0, 300, 300), new StandardRenderer()), 300, 350));
		
		RootPanel.get().add(flexTable);
	}
}
