package it.fabrizio.gori.particlesimulation.gwt.client.view;

import it.fabrizio.gori.particlesimulation.gwt.client.presenter.Presenter;

public interface View<T extends Presenter> {
	
	public T getPresenter();
}
