package it.fabrizio.gori.particlesimulation.gwt.client.presenter;

import it.fabrizio.gori.particlesimulation.Simulation;
import it.fabrizio.gori.particlesimulation.SimulationCreator;
import it.fabrizio.gori.particlesimulation.builder.SimulationBuilder;
import it.fabrizio.gori.particlesimulation.graphics.GraphicContext;
import it.fabrizio.gori.particlesimulation.graphics.Renderer;
import it.fabrizio.gori.particlesimulation.gwt.client.presenter.SimulationPresenter.SimulationView;
import it.fabrizio.gori.particlesimulation.gwt.client.view.View;

import com.google.gwt.animation.client.AnimationScheduler;
import com.google.gwt.animation.client.AnimationScheduler.AnimationCallback;

public class SimulationPresenter extends AbstractPresenter<SimulationView> implements AnimationCallback {
	
	private final Renderer renderer;
	private final Simulation simulation;
	private double currentTime = 0;
	
	public interface SimulationView extends View<SimulationPresenter> {
		
		public GraphicContext getGraphicContext();
	}

	public SimulationPresenter(SimulationBuilder simulationBuilder, Renderer renderer) {
		
		this.renderer = renderer;
		this.simulation = new SimulationCreator(simulationBuilder).createSimulation();
	}
	
	@Override
	public void onBind() {

		if(getView().getGraphicContext() != null) {
			
			renderer.setGraphicContext(getView().getGraphicContext());
			
			simulation.start(currentTime);
			simulation.draw(renderer);
			
			AnimationScheduler.get().requestAnimationFrame(this);
		}
	}
	
	@Override
	public void execute(double timestamp) {

		simulation.update(currentTime);
		simulation.draw(renderer);
		
		currentTime += (1000.0 / 60.0);
		
		if(!simulation.isCompleted()) {

			AnimationScheduler.get().requestAnimationFrame(this);
		}
	}
}
