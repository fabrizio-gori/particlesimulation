package it.fabrizio.gori.particlesimulation.gwt.client.view;

import it.fabrizio.gori.particlesimulation.graphics.GraphicContext;
import it.fabrizio.gori.particlesimulation.gwt.client.graphics.Html5ImplGraphicContext;
import it.fabrizio.gori.particlesimulation.gwt.client.presenter.SimulationPresenter;
import it.fabrizio.gori.particlesimulation.gwt.client.presenter.SimulationPresenter.SimulationView;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class SimulationViewImpl extends CompositeView<SimulationPresenter> implements SimulationView {
	
	private GraphicContext graphicContext = null;
	
	public SimulationViewImpl(SimulationPresenter presenter, int width, int height) {
		
		super(presenter);
		
		SimplePanel simplePanel = new SimplePanel();
		VerticalPanel verticalPanel = new VerticalPanel();
		
		verticalPanel.setSpacing(0);
		verticalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		
		if(Canvas.isSupported()) {

			Canvas canvas = Canvas.createIfSupported();
			
			canvas.setCoordinateSpaceWidth(width);
			canvas.setCoordinateSpaceHeight(height);
			
			verticalPanel.add(canvas);
			
			graphicContext = new Html5ImplGraphicContext(canvas);

		} else {
			
			verticalPanel.add(new Label("Browser non compatibile con HTML5 Canvas!"));
			verticalPanel.add(new Label("Per un elenco di browser compatibili consultare il link: "));
			verticalPanel.add(new HTML("<a href=\"http://caniuse.com/canvas\">http://caniuse.com/canvas</a>"));
		}
		
		simplePanel.setWidget(verticalPanel);
		
		initWidget(simplePanel);
	}

	@Override
	public GraphicContext getGraphicContext() {
		
		return graphicContext;
	}
}
