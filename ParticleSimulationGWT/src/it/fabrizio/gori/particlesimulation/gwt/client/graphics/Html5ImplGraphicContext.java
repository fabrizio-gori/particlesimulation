package it.fabrizio.gori.particlesimulation.gwt.client.graphics;

import it.fabrizio.gori.particlesimulation.graphics.Color;
import it.fabrizio.gori.particlesimulation.graphics.GraphicContext;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.Context2d.TextBaseline;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.canvas.dom.client.FillStrokeStyle;

public class Html5ImplGraphicContext implements GraphicContext {
	
	private final Context2d context;
	private int width, height;
	
	public Html5ImplGraphicContext(Canvas canvas) {
		
		this.context = canvas.getContext2d();
		
		this.width = canvas.getCoordinateSpaceWidth();
		this.height = canvas.getCoordinateSpaceHeight();
	}

	@Override
	public int getWidth() {
		
		return width;
	}

	@Override
	public int getHeight() {
		
		return height;
	}

	@Override
	public void drawCircle(double cx, double cy, double radius, boolean fill) {
		
		context.beginPath();
		context.arc(cx, cy, radius, 0, 2 * Math.PI);
		if(fill) {
			
			context.fill();
		}
		context.closePath();
	}

	@Override
	public void drawLine(double ax, double ay, double bx, double by) {
		
		context.beginPath();
		context.moveTo(ax, ay);
		context.lineTo(bx, by);
		context.stroke();
	}

	@Override
	public void drawText(String text, double x, double y, double ang) {

		context.save();
		context.setTextBaseline(TextBaseline.MIDDLE);
		context.translate(x, y);
		context.rotate(ang);
		context.fillText(text, 0, 0);
		context.restore();
	}

	@Override
	public void setColor(Color color) {
		
		FillStrokeStyle fillStyle = CssColor.make(color.getRed(), color.getGreen(), color.getBlue());
		
		context.setStrokeStyle(fillStyle);
		context.setFillStyle(fillStyle);
	}

	@Override
	public void clear() {
		
		context.clearRect(0, 0, width, height);
	}
}
